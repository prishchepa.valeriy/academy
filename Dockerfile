FROM golang:1.7.3
WORKDIR /root/
ADD https://gitlab.com/prishchepa.valeriy/academy/-/raw/master/main.go .
RUN go build main.go

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /root/main .
CMD ["./main"]  